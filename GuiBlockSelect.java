package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiBlockSelect extends GuiScreen {
	List<Integer> fullList = new ArrayList();
	List<Integer> invisible = new ArrayList();
	int listpos;

	public GuiBlockSelect() {
		this.listpos = 0;

		// Add all blocks to the list
		for (int i = 0; i < Block.blocksList.length; ++i) {
			if (Block.blocksList[i] == null)
				continue;
			fullList.add(new Integer(i));
		}
	}

	public void initGui() {
		// Add blacklisted blocks to invisible list
		for (int i = 0; i < mod_XRay.blackList.length; ++i)
			invisible.add(Integer.valueOf(mod_XRay.blackList[i]));
		Keyboard.enableRepeatEvents(true);
	}

	protected void actionPerformed(GuiButton guibutton) {
		if (guibutton.id != 0)
			invisible.add(new Integer(guibutton.id));
	}

	protected void keyTyped(char c, int key) {
		if (key == Keyboard.KEY_ESCAPE || key == Keyboard.KEY_BACK || key == Keyboard.KEY_LCONTROL || key == Keyboard.KEY_RCONTROL) {
			mc.displayGuiScreen(null);
			if (mod_XRay.on) mc.renderGlobal.loadRenderers();
		} else if (key == Keyboard.KEY_UP && listpos > 0) {
			listpos--;
		} else if (key == Keyboard.KEY_DOWN && listpos < fullList.size() - 1) {
			listpos++;
		} else if (key == Keyboard.KEY_RETURN && !invisible.remove(new Integer(fullList.get(listpos)))) {
			invisible.add(new Integer(fullList.get(listpos)));
		}
	}

	public void onGuiClosed() {
		mod_XRay.blackList = new int[invisible.size()];
		for (int i = 0; i < invisible.size(); ++i)
			mod_XRay.blackList[i] = ((Integer) invisible.get(i).intValue());
		mod_XRay.saveBlackList();
	}

	public void drawScreen(int i, int j, float f) {
		int w1 = width / 2 + 100;
		int w2 = width / 2 - 100;
		int h = height / 3;
		
		drawRect(w1 + 2, h + 12, w2 - 2, h - 12, 0xffc0c0c0); //border
		
		GuiButton key1 = new GuiButton(0, 20, 60, "XRay Key");
		key1.width=80;
		//controlList.add(key1);
		
		int i1 = listpos - 2;
		for (int j1 = 0; j1 < 7; j1++) {
			// If within block range
			if (i1 >= 0 && i1 < fullList.size()) {
				
				//Red box
				if (invisible.indexOf(new Integer(fullList.get(i1))) >= 0)
					drawRect(w1, ((h + 10) - 48) + j1 * 24, w2, (h - 10 - 48) + j1 * 24, 0xffff0000);
				
				//Id
				drawString(fontRenderer, (i1+1)+": ", width/2-66, (h - 60) + 7 + j1 * 24, 0xffffff);
				//Name
				drawCenteredString(fontRenderer,
						Block.blocksList[fullList.get(i1)].getBlockName().substring(5),
						width / 2,
						(h - 60) + 7 + j1 * 24,
						0xffffff);
				
				int texindex = Block.blocksList[fullList.get(i1)].blockIndexInTexture;
				if (texindex >= 0) {
					GL11.glPushMatrix();
					mc.renderEngine.bindTexture(mc.renderEngine.getTexture("/terrain.png"));
					drawTexturedModalRect(width / 2 - 97,
							(h - 60) + 4 + j1 * 24,
							(texindex % 16) * 16,
							(texindex / 16) * 16,
							16,
							16);
					GL11.glPopMatrix();
				}
			}
			i1++;
		}

		super.drawScreen(i, j, f);
	}
}