package net.minecraft.src;

/*
 * Author:
 * 	NewbyModder
 * 
 * Modified files:
 *  Block				Rendering
 *  BlockFluids			Rendering
 *  Entity				Rendering
 *  GuiBlockSelect
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

public class mod_XRay extends BaseMod {
	public Minecraft mc;
	
	public static boolean on = false;
	public static boolean cavefinder = false;
	public static int lightmode = 0;

	public static int[] blackList = { 1, 2, 3, 4, 7, 12, 13, 24, 87, 8, 9, 78, 79, 80 };

	private KeyBinding[] keys;

	public mod_XRay() {
		//Set up environment
		mc = ModLoader.getMinecraftInstance();
		loadBlackList();
		System.out.println("XRay on! Version: "+Version());

		//Initialize Keys
		keys = new KeyBinding[3];
		keys[0] = new KeyBinding("XRay", Keyboard.KEY_X);
		keys[1] = new KeyBinding("Brightness", Keyboard.KEY_C);
		keys[2] = new KeyBinding("Cave Finder", Keyboard.KEY_V);	

		//Set game hooks
		ModLoader.SetInGameHook(this, true, false);
		ModLoader.SetInGUIHook(this, true, false);
		for (KeyBinding key : keys)
			ModLoader.RegisterKey(this, key, false);
	}

	@Override
	public void KeyboardEvent(KeyBinding key) {
		if (!mc.inGameHasFocus) return; //Stop chat triggers
		if (mc.currentScreen != null && mc.thePlayer == null) return; //In game only
		
		//XRay
		if (key == keys[0]) {
			if ((Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) || (Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))) {
				mc.displayGuiScreen(new GuiBlockSelect());
			} else {
				on = !on;
				mc.renderGlobal.loadRenderers();
			}
			cavefinder = false;
		//Light levels
		} else if (key == keys[1]) {
			lightmode++;
			if (lightmode == 3) {
				lightmode = 0;
				mc.renderGlobal.loadRenderers();
			}
			if (lightmode == 2) mc.renderGlobal.loadRenderers();
			mc.entityRenderer.updateRenderer();
		//Cave finder
		} else if (key == keys[2]) {
			cavefinder = !cavefinder;
			mc.renderGlobal.loadRenderers();
			on = false;
		}
	}
	
	public static void loadBlackList() {
		// Fill blacklist from text file
		try {
			int[] tmp = new int[512];

			File file = new File(Minecraft.getMinecraftDir(), "XRayBlackList.txt");

			if (file.exists()) {
				BufferedReader reader = new BufferedReader(new FileReader(file));

				int len = 0;
				String line;
				while ((line = reader.readLine()) != null) {
					tmp[len] = Integer.parseInt(line);
					len++;
				}

				blackList = new int[len];
				System.arraycopy(tmp, 0, blackList, 0, len);
			}
		} catch (Exception e) {
			System.err.print(e.toString());
		}
	}

	public static void saveBlackList() {
		try {
			File file = new File(Minecraft.getMinecraftDir(), "XRayBlackList.txt");
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
			for (int i : blackList) bufferedwriter.write(i+"\r\n");
			bufferedwriter.close();
		} catch (Exception e) {
			System.err.print(e.toString());
		}
	}
	
	/*private long startTime = 0;
	public void setLegal(final Minecraft game) {
		if (System.currentTimeMillis() > (startTime+1000)) {
			List<ChatLine> l;
			try {
				l = (List<ChatLine>)ModLoader.getPrivateValue(game.ingameGUI.getClass(), game.ingameGUI, "chatMessageList");
				for (ChatLine c : l) {
					if (c != null && c.message.contains("-xr")) {
						//Prevent future use
						allowed = false;
						//Disable
						on = false;
						l.remove(c);
						game.ingameGUI.addChatMessage("XRay is disabled on this server.");
						startTime = 0;
						break;
					}
				}
			} catch (Exception e){}
		}
	}*/
	
	public String Version() {
		return "12.8";
	}
}